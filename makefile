all : rapport.pdf  presentation.pdf handout.pdf
	echo "done"

rapport.pdf : rapport.tex rapport.bib 
	lualatex -shell-escape rapport.tex
	bibtex rapport
	lualatex -shell-escape rapport.tex
	lualatex -shell-escape rapport.tex

handout.pdf : soutenance.tex handout.tex handout.bib
	lualatex -shell-escape handout.tex
	bibtex handout 
	lualatex -shell-escape handout.tex
	lualatex -shell-escape handout.tex

presentation.pdf : soutenance.tex presentation.tex presentation.bib
	lualatex -shell-escape presentation.tex
	bibtex presentation
	lualatex -shell-escape presentation.tex
	lualatex -shell-escape presentation.tex

rapport.bib : biblio.bib
	cp biblio.bib rapport.bib

poster.bib : biblio.bib
	cp biblio.bib poster.bib

presentation.bib : biblio.bib
	cp biblio.bib presentation.bib

handout.bib : biblio.bib
	cp biblio.bib handout.bib

clean : clean-rapport clean-poster clean-handout clean-presentation
	rm -f rapport.pdf
	rm -f poster.pdf
	rm -f handout.pdf
	rm -f presentation.pdf

clean-rapport :
	rm -f rapport.aux
	rm -f rapport.bbl
	rm -f rapport.bib
	rm -f rapport.blg
	rm -f rapport.out
	rm -f rapport.log
	rm -f rapport.toc
	rm -f rapport.lof
	rm -f rapport.lot
	rm -f rapport.lol
	rm -f rapport.ist
	rm -f rapport.glg
	rm -f rapport.gls
	rm -f rapport.glo
	rm -f texput.log
	rm -f -r _minted-rapport

clean-poster :
	rm -f poster.aux
	rm -f poster.bbl
	rm -f poster.bib
	rm -f poster.blg
	rm -f poster.out
	rm -f poster.log
	rm -f poster.toc
	rm -f poster.lof
	rm -f poster.lot
	rm -f poster.lol
	rm -f poster.ist
	rm -f poster.glg
	rm -f poster.gls
	rm -f poster.glo
	rm -f texput.log
	rm -f -r _minted-poster

clean-handout :
	rm -f handout.aux
	rm -f handout.bbl
	rm -f handout.bib
	rm -f handout.blg
	rm -f handout.out
	rm -f handout.log
	rm -f handout.toc
	rm -f handout.lof
	rm -f handout.lot
	rm -f handout.lol
	rm -f handout.ist
	rm -f handout.glg
	rm -f handout.gls
	rm -f handout.glo
	rm -f handout.nav
	rm -f handout.snm
	rm -f handout.vrb
	rm -f texput.log
	rm -f -r _minted-handout

clean-presentation :
	rm -f presentation.aux
	rm -f presentation.bbl
	rm -f presentation.bib
	rm -f presentation.blg
	rm -f presentation.out
	rm -f presentation.log
	rm -f presentation.toc
	rm -f presentation.lof
	rm -f presentation.lot
	rm -f presentation.lol
	rm -f presentation.ist
	rm -f presentation.glg
	rm -f presentation.gls
	rm -f presentation.glo
	rm -f presentation.nav
	rm -f presentation.snm
	rm -f presentation.vrb
	rm -f texput.log
	rm -f -r _minted-presentation
